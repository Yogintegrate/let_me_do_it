import sys
import os
import yaml
import datetime
import subprocess

profile_file = '/resources/profile.yaml'
skipped = False

# todo modify to print Exception also
def log(output):
    print '[' + str(datetime.datetime.now().time()) + ']  ' + str(output)

def get_cmd_arg():
    cmd_arg = {}
    for ar in sys.argv[1:]:
        key, value = ar.split(':')
        cmd_arg[key] = value
    log('Command line argument processed successfully')
    return cmd_arg

def get_yaml_params(path='/resources/profile.yaml'):
    with open(str(os.getcwd())+path) as stream:
        return (yaml.load(stream))

cmd_arg = get_cmd_arg()
file_arg = get_yaml_params(profile_file)

def select_profile():
    if 'profile' in cmd_arg.iterkeys():
        log('Select Profile - ' + cmd_arg['profile'])
        return file_arg[cmd_arg['profile']]
    else:

        return select_probable_profile()

def select_probable_profile():
    current_time = datetime.datetime.now()
    work = current_time.replace(hour=10, minute=30, second=0)
    home = current_time.replace(hour=20, minute=15, second=0)
    if current_time > work and current_time < home:
        cmd_arg['profile'] = 'work'
        log('Select Profile - ' + cmd_arg['profile'])
    else:
        cmd_arg['profile'] = 'home'
        log('Select Profile - ' + cmd_arg['profile'])
    return file_arg[cmd_arg['profile']]

# todo for linux
def chrome(app_params):
    log('Executing Chrome')
    tabs = app_params['arguments'].replace('||', ' ')
    action = 'start'
    app = 'chrome'
    command = action + ' ' + app + ' ' + tabs
    return os.system(command)

def skype(app_params):
    log('Executing Skype')
    path = app_params['path']
    return subprocess.call([path])

def virtual_machine(app_params):
    log('Executing Virtual Machine')
    path = app_params['path']
    params = app_params['arguments']
    app = 'startvm'
    command = '"'+path+'"' + ' ' + app + ' ' + params
    return os.system(command)
    #return subprocess.call([path,app+' '+params])

# todo implement callback
def ostoto(app_params):
    log('Executing OS Toto')
    path = app_params['path']
    return subprocess.call([path])


def popcorn(app_params):
    log('Executing Popcorn time')
    path = app_params['path']
    return subprocess.call([path])



def execute_command(command):
    if command == 0:
        return 0


later = {}

def compose_commmand(application, skipped = skipped):
    for application_name in application:
        if application_name == 'chrome':
            return chrome(application[application_name])
        if application_name == 'skype':
            return skype(application[application_name])
        if application_name == 'virtual_machine':
            return virtual_machine(application[application_name])
        if skipped == True and application_name == 'ostoto':
            return ostoto(application[application_name])
        if application_name == 'ostoto' and skipped == False:
            #return ostoto(application[application_name])
            skipped = True
            later[application_name] = application[application_name]
            return 0
        if application_name == 'popcorn':
            return popcorn(application[application_name])
        return 'application_not_found'


def run():
    profile = select_profile()
    for key,value in profile.iteritems():
        try:
            application = {key: value}
            command = compose_commmand(application)
            execute_command(command)
            log(key + ' execution successful.')
        except Exception as e:
            log(key)
            log('Exception')

    for key,value in later.iteritems():
        try:
            application = {key: value}
            command = compose_commmand(application, True)
            execute_command(command)
            log(key + ' execution successful.')
        except Exception as e:
            log('Exception')

run()