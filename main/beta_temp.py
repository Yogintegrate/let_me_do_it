import subprocess
from datetime import datetime
import os
import yaml

class BetaTemp:
    parameters = None

    @staticmethod
    def get_yaml_params():
        with open(str(os.getcwd())+'/resources/beta_temp.yaml') as stream:
            return (yaml.load(stream))

    def __init__(self):
        self.parameters = BetaTemp.get_yaml_params()

    def is_weekend(self):
        if datetime.today().weekday() >= 5:
            return True
        else:
            return False

    def profile_for_timing(self):
        current_time = datetime.now().time() #todo I guess
        for key,value in self.parameters.items():
            #t = value['timing']['start_time']
            #s = datetime.strptime(value['timing']['start_time'],'%H:%M:%S').time()
            #e = datetime.strptime(value['timing']['end_time'],'%H:%M:%S')
            if current_time > datetime.strptime(value['timing']['start_time'],'%H:%M:%S').time() and current_time < datetime.strptime(value['timing']['end_time'],'%H:%M:%S').time():
                return key
        return 'profile_for_timing_not_found'

    def is_holiday(self):
        return False # find holidays according to timezone from calender

    def predict_profile(self):#todo remove dependencies of keywords work , home etc
        profile_timing = self.profile_for_timing()
        if not self.is_weekend() and not self.is_holiday() and profile_timing == 'work':
            return 'work'
        else:
            return profile_timing

    def get_map_of_programs(self):
        profile = self.parameters[self.predict_profile()]
        applications = profile['application']
        for key, value in applications.iteritems(): #todo , all application running ( i do not think so ) , close python after that
            subprocess.call(value)

betaTemp = BetaTemp()
betaTemp.get_map_of_programs()